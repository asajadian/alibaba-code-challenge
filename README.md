# Alibaba Code Challenge

This project is the solution to coding challenge requested by Alibaba Group company as part of hiring process.

# Demo

The project is depolyed to https://mystifying-knuth-200794.netlify.app/

# Components

As stated in challenge requirements we have 3 components.
We have a `BaseTextInput` which is basically an input component with custom stylings, nothing special here.

We also have a `List` component which is responsible for rendering a list. This component receives an `items` prop which is assumed to be an array of objects each of which must have two properties, `id` and `label`. `id` values must be unique among all items.

Last but not least is the `TagInput`. List of tags are fetched from an imaginary API. The list of tags are shown as the input gets focus. As the user inputs characters into the input field the list is filtered.

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

#Libraries/Tools used
To mock API request: `miragejs`
To make http requests: `axios`
Grid system and minor styling: `bulma`
