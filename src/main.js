import Vue from 'vue';
import axios from 'axios';
import App from './App.vue';
import makeServer from './server';
import { clickOutside } from './utils/directives';
import './assets/css/main.css';

makeServer();
Vue.config.productionTip = false;

Vue.directive('click-outside', clickOutside);
Vue.prototype.$axios = axios;

new Vue({
  render: h => h(App)
}).$mount('#app');
